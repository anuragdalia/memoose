package com.memoose.sample

import com.adap.tools.CacheClient
import com.adap.tools.InMemoryCache
import com.adap.tools.Memoose
import com.adap.tools.generateString
import com.adap.tools.memoose.Memoosed
import com.memoose.sample.Sample.testFunction
import redis.clients.jedis.Jedis

public object SampleApp {
    @JvmStatic
    public fun main(args: Array<String>) {
        Memoosed.cache = JedisCache(Jedis("localhost"))

        println("original : " + Sample.testFunction("a"))
        println("original : " + Sample.testFunction("a"))
        println("original : " + Sample.testFunction("b"))
        println("original : " + Sample.testFunction("b"))
        println("original : " + Sample.testFunction("c"))
        println("original : " + Sample.testFunction("c"))

        println("cache : " + Memoosed.Sample.testFunction("a"))
        println("cache : " + Memoosed.Sample.testFunction("a"))
        println("cache : " + Memoosed.Sample.testFunction("a"))
        println("cache : " + Memoosed.Sample.testFunction("a"))
        println("cache : " + Memoosed.Sample.testFunction("a"))
        println("demoized")
        Memoosed.Sample.testFunctionDemoize("a")
        println("cache : " + Memoosed.Sample.testFunction("a"))
        println("cache : " + Memoosed.Sample.testFunction("a"))
        println("cache : " + Memoosed.Sample.testFunction("a"))
        println("cache : " + Memoosed.Sample.testFunction("a"))
        println("cache : " + Memoosed.Sample.testFunction("a"))
        println()
        println("cache : " + Memoosed.Sample2.testFunction(DAFAQ()))
        println("cache : " + Memoosed.Sample2.testFunction(DAFAQ()))
        println("cache : " + Memoosed.Sample2.testFunction(DAFAQ()))
        println("cache : " + Memoosed.Sample2.testFunction(DAFAQ()))
    }
}

class wtf() {
    @Memoose()
    suspend fun testFunction(sample: String): List<String> {
        return arrayListOf(sample + "__" + generateString(6))
    }
}

object Sample {
    @Memoose()
    fun testFunction(sample: String): List<String> {
        return arrayListOf(sample + "__" + generateString(6))
    }

    @Memoose()
    fun testFunction1(sample: String): Wttf<String, List<String>> {
        return Wttf("", arrayListOf(sample + "__" + generateString(6)))
    }
}

object Sample2 {
    @Memoose()
    fun testFunction(sample: DAFAQ): List<String> {
        return arrayListOf(sample.toString() + "__" + generateString(6))
    }

    @Memoose()
    fun testFunction1(sample: String): Wttf<String, List<String>> {
        return Wttf("", arrayListOf(sample + "__" + generateString(6)))
    }
}

class DAFAQ()
class Wttf<T, K>(val a: T, val b: K)