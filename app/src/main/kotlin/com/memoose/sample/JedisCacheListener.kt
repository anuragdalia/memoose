package com.memoose.sample

import com.adap.tools.MemooseListener

@MemooseListener
class JedisCacheListener() {

    fun logApiFailure() {
        println("Api Failed")
    }

    fun logReturnedUnit() {
        println("Unit")
    }
}
