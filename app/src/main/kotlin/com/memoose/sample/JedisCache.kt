package com.memoose.sample;

import com.adap.tools.MemooseCache
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import redis.clients.jedis.Jedis

@MemooseCache
class JedisCache(val jedis: Jedis) {
    fun delete(key: String) {
        jedis.del(key)
    }

    fun exists(key: String): Boolean {
        return jedis.exists(key)
    }

    inline fun <reified T> get(key: String): T? {
        return jedis.get(key)?.let { Json.decodeFromString(it) }
    }

    inline fun <reified T> set(key: String, value: T): T {
        jedis.set(key, Json.encodeToString(value))
        return value
    }

    inline fun <reified T> setEx(key: String, expiry: Int, value: T): T {
        jedis.setex(key, expiry, Json.encodeToString(value))
        return value
    }
}