import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    application
    kotlin("jvm")
    kotlin("kapt")
    kotlin("plugin.serialization")
}

repositories {
    mavenLocal()
    jcenter()
    maven("https://kotlin.bintray.com/ktor")
    maven("https://kotlin.bintray.com/kotlin-js-wrappers")
    maven("https://kotlin.bintray.com/kotlinx")
    maven("https://dl.bintray.com/adhesivee/oauth2-server")
}

application {
    mainClass.set("com.memoose.sample.SampleApp")
    mainClassName = "com.memoose.sample.SampleApp"
}

group = "com.adap.server"
version = "0.2"

dependencies {
    implementation(project(":memoose-annotations"))
    kapt(project(":memoose-kapt"))

    implementation("redis.clients:jedis:3.6.3")
    implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:1.2.2")
}

tasks.withType<KotlinCompile> { kotlinOptions { jvmTarget = "1.8" } }