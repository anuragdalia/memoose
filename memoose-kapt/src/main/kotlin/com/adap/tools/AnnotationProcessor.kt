@file:OptIn(KotlinPoetMetadataPreview::class)

package com.adap.tools

import com.google.auto.service.AutoService
import com.squareup.kotlinpoet.*
import com.squareup.kotlinpoet.metadata.KotlinPoetMetadataPreview
import com.squareup.kotlinpoet.metadata.specs.toFileSpec
import com.squareup.kotlinpoet.metadata.specs.toTypeSpec
import com.squareup.kotlinpoet.metadata.toImmutableKmClass
import java.io.File
import javax.annotation.processing.*
import javax.lang.model.SourceVersion
import javax.lang.model.element.Element
import javax.lang.model.element.ElementKind
import javax.lang.model.element.ExecutableElement
import javax.lang.model.element.TypeElement
import javax.lang.model.type.*


val ensureFunc = FunSpec.builder("ensureCache")
    .addCode(
        """
            if (cache == null) throw RuntimeException("Make sure Memoosed.cache is set")
            //if (cache !is CacheClient) 
            //    throw RuntimeException("Provided cacheclient should extend #CacheClient interface")
            """.trimIndent()
    ).build()

@SupportedSourceVersion(SourceVersion.RELEASE_8)
@AutoService(Processor::class)
class AnnotationProcessor : AbstractProcessor() {
    private var processingEnvironment: ProcessingEnvironment? = null
    override fun init(processingEnv: ProcessingEnvironment?) {
        super.init(processingEnv)
        processingEnvironment = processingEnv
    }

    fun getExtracted(ve: TypeElement): TypeElement? {
        val typeMirror: TypeMirror = ve.asType();
        val element: Element = processingEnvironment?.typeUtils?.asElement(typeMirror) ?: return null

        // instanceof implies null-ckeck
        return if (element is TypeElement) element else null
    }

    private val subclassMap = mutableMapOf<String, TypeSpec.Builder>()

    override fun getSupportedAnnotationTypes() = mutableSetOf(
        Memoose::class.java.canonicalName,
        MemooseCache::class.java.canonicalName,
        MemooseListener::class.java.canonicalName
    )

    override fun process(annotations: MutableSet<out TypeElement>?, roundEnv: RoundEnvironment): Boolean {

        val cacheType = roundEnv.getElementsAnnotatedWith(MemooseCache::class.java)
        if (cacheType.isEmpty()) return false

        val logHelper = roundEnv.getElementsAnnotatedWith(MemooseListener::class.java)
        if (logHelper.isEmpty()) return false

        val elements = roundEnv.getElementsAnnotatedWith(Memoose::class.java)
        if (elements.isEmpty()) return false

        val fileName = "Memoosed"
        val fileBuilder = FileSpec.builder("com.adap.tools.memoose", fileName)
        val objectBuilder = TypeSpec.objectBuilder(fileName)

        elements.forEach(this@AnnotationProcessor::processAnnotation)

        subclassMap.values.forEach { objectBuilder.addType(it.build()) }
        val classs = cacheType.first() as TypeElement
        val logHelperClass = logHelper.first() as TypeElement

        val cacheProperty =
            PropertySpec.builder(
                "cache",
                ClassName(
                    classs.toFileSpec().packageName,
                    classs.toTypeSpec().name ?: return false
                ).copy(nullable = true)
            )
                .mutable(true)
                .addModifiers(KModifier.PUBLIC)
                .initializer("null")
                .build()

        val logUnitFunction =
            PropertySpec.builder(
                "logUnit",
                ClassName(
                    logHelperClass.toFileSpec().packageName,
                    logHelperClass.toTypeSpec().name ?: return false
                ).copy(nullable = true)
            )
                .mutable(true)
                .addModifiers(KModifier.PUBLIC)
                .initializer("null")
                .build()

        objectBuilder.addProperty(cacheProperty)
        objectBuilder.addProperty(logUnitFunction)

        fileBuilder.addImport("com.adap.tools", "CacheClient")
        val file = fileBuilder.addType(objectBuilder.build()).build()

        val kaptKotlinGeneratedDir =
            processingEnv.options[KAPT_KOTLIN_GENERATED_OPTION_NAME] ?: throw RuntimeException("dafaq")
        file.writeTo(File(kaptKotlinGeneratedDir))
        return true
    }

    override fun getSupportedSourceVersion(): SourceVersion = SourceVersion.latest()

    private fun processAnnotation(element: Element) {
        if (element.kind == ElementKind.METHOD) {

            val magic = MagicMushroom(element)

            var subclass: TypeSpec.Builder? = subclassMap[magic.qualifiedClassName]
            if (subclass == null) {
                subclassMap[magic.qualifiedClassName] = TypeSpec.objectBuilder(magic.simpleClassName)
                    .addFunction(ensureFunc)
            }
            subclass = subclassMap[magic.qualifiedClassName]

            subclass?.addFunction(magic.memoizeFunction().build())
            subclass?.addFunction(magic.demoizeFunction().build())
            subclass?.addFunction(magic.cacheKeyFunction().build())
            subclass?.addFunction(magic.refreshFunction().build())
            subclass?.addFunction(magic.executeFunction().build())
            subclass?.addFunction(magic.existsFunction().build())

//            subclass?.addFunction(set())
//            subclass?.addFunction(expiry())
        }
    }

    companion object {
        const val KAPT_KOTLIN_GENERATED_OPTION_NAME = "kapt.kotlin.generated"
    }
}

class MagicMushroom(element: Element) {

    private val expiry = element.getAnnotation(Memoose::class.java).expiry

    private val method = (element as ExecutableElement)

    private val classElement = element.enclosingElement as TypeElement
    val classSpec = classElement.toTypeSpec()

    private val methodName = method.simpleName.toString()
    private val methodSpec = classSpec
        .funSpecs
        .find { it.name == methodName }!!

    val kmFunction = classElement
        .toImmutableKmClass()
        .functions
        .find { it.name == methodName }!!

    init {
        if (methodSpec.returnType == null)
            throw RuntimeException("return type of a memoosed function cannot be unit")
    }

    private val fileSpec = classElement.toFileSpec()
    val qualifiedClassName = classElement.qualifiedName.toString()
    private val packageName = fileSpec.packageName
    val simpleClassName = classElement.simpleName.toString()
    private val isClass = classSpec.kind == TypeSpec.Kind.CLASS
    private val selfObjectName = simpleClassName.apply { get(0).toLowerCase() }
    private val selfObjectParameterSpec = ParameterSpec(selfObjectName, ClassName(packageName, simpleClassName))

    private val functionSpecificUniqueString = generateString(6)

    private val paramsForExec =
        methodSpec.parameters.toMutableList().apply { if (isClass) add(selfObjectParameterSpec) }
            .joinToString(",") { it.name }
    private val paramsForExecOriginalObject =
        methodSpec.parameters.toMutableList().joinToString(",") { it.name }


    private fun addExtraParam(funSpec: FunSpec.Builder): FunSpec.Builder = funSpec.apply {
        if (isClass) addParameter(selfObjectParameterSpec)
    }

    fun memoizeFunction(): FunSpec.Builder {
        val funBuilder = methodSpec.toBuilder(methodName).returns(methodSpec.returnType!!)
        funBuilder.clearBody()
        funBuilder.addCode(
            """
                ensureCache()
                val cacheKey = com.adap.tools.toMD5(${paramsForCacheKey()})
                val fromCache: Any? = cache!!.get(cacheKey)
                if(fromCache == null || fromCache is Unit) {
                    if (fromCache is Unit) logUnit?.logReturnedUnit()
                    val value = ${if (isClass) selfObjectName else qualifiedClassName}.${methodName}($paramsForExecOriginalObject)
                    ${if (expiry == -1) "cache!!.set(cacheKey, value)" else "cache!!.setEx(cacheKey, $expiry, value)"}
                    return value
                }
                return fromCache as ${methodSpec.returnType?.copy(nullable = false)}
            """.trimIndent()
        )
        return addExtraParam(funBuilder)
    }

    fun demoizeFunction(): FunSpec.Builder {
        val funBuilder = methodSpec.toBuilder("${methodName}Demoize").returns(UNIT)
        funBuilder.clearBody()
        funBuilder.addCode(
            """
                ensureCache()
                val cacheKey = com.adap.tools.toMD5(${paramsForCacheKey()})
                return cache!!.delete(cacheKey)
            """.trimIndent()
        )
        return addExtraParam(funBuilder)
    }

    fun cacheKeyFunction(): FunSpec.Builder {
        val funBuilder = methodSpec.toBuilder("${methodName}CacheKey").returns(STRING)
        funBuilder.clearBody()
        funBuilder.addCode(
            """
                ensureCache()
                val cacheKey = com.adap.tools.toMD5(${paramsForCacheKey()})
                return cacheKey
            """.trimIndent()
        )
        return addExtraParam(funBuilder)
    }

    fun refreshFunction(): FunSpec.Builder {
        val funBuilder = methodSpec.toBuilder("${methodName}Refresh").returns(methodSpec.returnType ?: UNIT)
        funBuilder.clearBody()
        funBuilder.addCode(
            """
                ${methodName}Demoize($paramsForExec)
                return ${methodName}($paramsForExec)
            """.trimIndent()
        )
        return addExtraParam(funBuilder)
    }

    fun executeFunction(): FunSpec.Builder {
        val funBuilder = methodSpec.toBuilder("${methodName}Execute").returns((methodSpec.returnType ?: UNIT))
        funBuilder.clearBody()
        funBuilder.addCode(
            """
                ensureCache()
                return ${if (isClass) selfObjectName else qualifiedClassName}.${methodName}($paramsForExecOriginalObject)
            """.trimIndent()
        )
        return addExtraParam(funBuilder)
    }

    fun existsFunction(): FunSpec.Builder {
        val funBuilder = methodSpec.toBuilder("${methodName}Exists").returns(BOOLEAN)
        funBuilder.clearBody()
        funBuilder.addCode(
            """
                ensureCache()
                val cacheKey = com.adap.tools.toMD5(${paramsForCacheKey()})
                return cache!!.exists(cacheKey)
            """.trimIndent()
        )
        return addExtraParam(funBuilder)
    }

    private fun paramsForCacheKey(): String {
        val demoizeCacheKeyGeneratorArray =
            mutableListOf("\"${functionSpecificUniqueString}\"", "\"${methodName}\"")
        demoizeCacheKeyGeneratorArray.addAll(funcParams())
        return demoizeCacheKeyGeneratorArray.joinToString("+")
    }

    private fun funcParams(): List<String> = methodSpec.parameters.mapIndexed { index, paramFromSpec ->
        val parameter = method.parameters.getOrNull(index) ?: return@mapIndexed null
        val isDeclaredType = parameter.asType() is DeclaredType
        val isId = if (isDeclaredType) {
            ((parameter.asType() as DeclaredType).asElement() as TypeElement)
                .interfaces
                .map { interfaceObjects -> interfaceObjects.toString() }
                .contains("com.adap.tools.Identifiable")
        } else false
        val nulll = if (paramFromSpec.type.isNullable) "?" else ""
        val paramName = parameter.simpleName
        val execFunction = if (isId) "id()" else "toString()"
        return@mapIndexed "$paramName$nulll.$execFunction"
    }.filterNotNull()
}