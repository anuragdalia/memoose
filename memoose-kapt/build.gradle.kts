plugins {
    `java-library`
    kotlin("jvm")
    kotlin("kapt")
    `maven-publish`
}

group = "com.adap.tools"
version = "0.0.1"

repositories {
    mavenCentral()
}

dependencies {
    implementation(project(":memoose-annotations"))
    implementation("com.squareup:kotlinpoet:1.9.0")
    implementation("com.squareup:kotlinpoet-metadata:1.9.0")
    implementation("com.squareup:kotlinpoet-metadata-specs:1.9.0")
    implementation("com.google.auto.service:auto-service:1.0")
    kapt("com.google.auto.service:auto-service:1.0")
}

publishing {
    publications {
        create<MavenPublication>("maven") {
            groupId = "com.adap.tools"
            artifactId = "memoose-annotation-processor"
            version = "0.0.1"
            from(components["kotlin"])
        }
    }
}