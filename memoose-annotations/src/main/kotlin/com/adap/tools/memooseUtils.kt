package com.adap.tools

import java.security.MessageDigest

private val charPool: List<Char> = ('a'..'z') + ('A'..'Z') + ('0'..'9')
fun generateString(length: Int = 16): String {
    return (1..length)
        .map { kotlin.random.Random.nextInt(0, charPool.size) }
        .map(charPool::get)
        .joinToString("");
}

fun toMD5(string: String): String {
    val bytes = MessageDigest.getInstance("MD5").digest(string.toByteArray())
    return bytes.joinToString("") { "%02x".format(it) }
}