package com.adap.tools

interface Identifiable {
    fun id(): String {
        return toString()
    }
}