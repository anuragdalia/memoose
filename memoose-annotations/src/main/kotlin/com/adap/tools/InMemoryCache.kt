package com.adap.tools

class InMemoryCache() : CacheClient {
    private val store = mutableMapOf<String, Any>()
    override fun <T> set(key: String, value: T): T {
        store[key] = value as Any
        return value
    }

    override fun <T> setEx(key: String, expiry: Int, value: T): T {
        print("Expiry isn't honored in InMemoryCache. Will be stored indefinitely.")
        store[key] = value as Any
        return value
    }

    override fun <T> get(key: String): T? {
        return store[key] as T?
    }

    override fun delete(key: String) {
        store.remove(key)
    }

    override fun exists(key: String): Boolean {
        return store[key] != null
    }
}