package com.adap.tools

interface CacheClient {
    fun <T> set(key: String, value: T): T
    fun <T> setEx(key: String, expiry: Int, value: T): T
    fun <T> get(key: String): T?
    fun delete(key: String)
    fun exists(key: String): Boolean
}