plugins {
    kotlin("jvm")
    `maven-publish`
}

group = "com.adap.tools"
version = "0.0.1"

repositories {
    mavenCentral()
}

dependencies {
}


publishing {
    publications {
        create<MavenPublication>("maven") {
            groupId = "com.adap.tools"
            artifactId = "memoose"
            version = "0.0.1"
            from(components["kotlin"])
        }
    }
}