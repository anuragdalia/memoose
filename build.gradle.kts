
plugins {
    kotlin("jvm") version "1.5.30"
    kotlin("kapt") version "1.5.30"
    kotlin("plugin.serialization") version "1.5.30"
}

allprojects {
    repositories {
        mavenLocal()
        google()
        jcenter()
        mavenCentral()
    }
}