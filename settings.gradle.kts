rootProject.name = "memoose"

pluginManagement {
    repositories {
        mavenCentral()
        gradlePluginPortal()
        google()
        jcenter()
        maven("https://dl.bintray.com/kotlin/kotlin-eap")
        maven("https://dl.bintray.com/kotlin/kotlin-dev")
    }
}
enableFeaturePreview("GRADLE_METADATA")
include(":app", ":memoose-kapt", ":memoose-annotations")
